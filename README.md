# indieweb-algorithms [![Hackage](https://img.shields.io/hackage/v/indieweb-algorithms.svg?style=flat)](https://hackage.haskell.org/package/indieweb-algorithms) [![unlicense](https://img.shields.io/badge/un-license-green.svg?style=flat)](https://unlicense.org)

A collection of implementations of [IndieWeb]- and [Microformats 2]-related algorithms (based on [microformats2-parser] and [http-link-header]):

- finding all microformats of a given type (while retaining the path to them), ie. flattening the tree
- discovering **[authorship](https://indieweb.org/authorship)** of an `h-entry`
  - discovering the [representative h-card](https://microformats.org/wiki/representative-h-card-parsing)
- discovering [Webmention](https://indieweb.org/Webmention)/[Micropub](https://indieweb.org/Micropub)/[IndieAuth](https://indieweb.org/IndieAuth)/etc. **endpoints** (HTTP `Link` header, `a` and `link` tags with the `rel` attribute)
- parsing tweets on Twitter.com into microformats (like [php-mf2-shim](https://github.com/indieweb/php-mf2-shim))
- detecting [post types](httpss://indieweb.org/posts#Kinds_of_Posts)
- converting microformats to Atom feeds

[IndieWeb]: https://indieweb.org
[Microformats 2]: https://microformats.org/wiki/microformats2
[microformats2-parser]: https://codeberg.org/valpackett/microformats2-parser
[http-link-header]: https://codeberg.org/valpackett/http-link-header

## Usage

### Endpoints

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Network.HTTP.Link
import Data.Default
import Data.Maybe
import Data.Microformats2.Parser
import Data.IndieWeb.Endpoints

discoverEndpoints [ "micropub" ] (parseMf2 def $ documentRoot $ parseLBS "<link rel=micropub href='http://example.com/micropub2'>...") (fromMaybe [] $ parseLinkHeader "<http://example.com/micropub>; rel=\"micropub\"")
```

## Development

Use [stack] to build.  
Use ghci to run tests quickly with `:test` (see the `.ghci` file).

```bash
$ stack build

$ stack test

$ stack ghci
```

[stack]: https://github.com/commercialhaskell/stack

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the `UNLICENSE` file or [unlicense.org](https://unlicense.org).
